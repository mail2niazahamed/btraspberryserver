package com.wild.detect.sample;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

import javax.bluetooth.DeviceClass;
import javax.bluetooth.DiscoveryAgent;
import javax.bluetooth.DiscoveryListener;
import javax.bluetooth.LocalDevice;
import javax.bluetooth.RemoteDevice;
import javax.bluetooth.ServiceRecord;
import javax.bluetooth.UUID;
import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;
import javax.microedition.io.StreamConnectionNotifier;
import javax.obex.ClientSession;
import javax.obex.HeaderSet;
import javax.obex.Operation;
import javax.obex.ResponseCodes;

public class OBEXPutServerAndroidChat {

	static final String serverUUID = "11111111111111111111111111111123";

	public static void main(String[] args) throws IOException {
		LocalDevice localDevice = LocalDevice.getLocalDevice();

		localDevice.setDiscoverable(DiscoveryAgent.GIAC); // Advertising the
															// service

		String url = "btspp://localhost:" + serverUUID + ";name=BlueToothServer";
		StreamConnectionNotifier server = (StreamConnectionNotifier) Connector.open(url);
		StreamConnection connection = null;
		while (true) {
			connection = server.acceptAndOpen(); // Wait until
			RemoteDevice device = RemoteDevice.getRemoteDevice(connection);
			System.out.println("Remote device address: " + device.getBluetoothAddress());
			// System.out.println("Remote device isAuthenticated: " +
			// device.isAuthenticated());
			// System.out.println("Remote device isAuthorized(connection): " +
			// device.isAuthorized(connection));
			// System.out.println("Remote device name: " +
			// device.getFriendlyName(true));
			// client
			// connects
			// === At this point, two devices should be connected ===//
			UUID serviceUUID = new UUID(0x1105);
			UUID[] searchUuidSet = new UUID[] { serviceUUID };
			int[] attrIDs = new int[] { 0x0100 // Service name
			};
			LocalDevice.getLocalDevice().getDiscoveryAgent().searchServices(attrIDs, searchUuidSet, device,
					new MyDeviceDiscovery(connection));
		}
	}

	static class MyDeviceDiscovery implements DiscoveryListener {
		StreamConnection connection;

		public MyDeviceDiscovery(StreamConnection connection) {
			this.connection = connection;
		}

		@Override
		public void servicesDiscovered(int arg0, ServiceRecord[] servRecord) {
			// TODO Auto-generated method stub
			boolean isServiceURLFound = false;
			for (int i = 0; i < servRecord.length; i++) {
				String url = servRecord[i].getConnectionURL(ServiceRecord.NOAUTHENTICATE_NOENCRYPT, false);
				if (url == null) {
					continue;
				} else {
					isServiceURLFound = true;
					System.out.println("calling Client Thread...");
					new ClientThread(connection, url).run();
					break;
				}

			}
			System.out.println("Service URL Found status::" + isServiceURLFound);
		}

		@Override
		public void serviceSearchCompleted(int arg0, int arg1) {
			// TODO Auto-generated method stub

		}

		@Override
		public void inquiryCompleted(int arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void deviceDiscovered(RemoteDevice arg0, DeviceClass arg1) {
			// TODO Auto-generated method stub

		}
	}

	static class ClientThread implements Runnable {
		StreamConnection connection;
		DataInputStream dis;
		DataOutputStream dos;
		PrintWriter pWriter;
		String url;

		public ClientThread(StreamConnection connection, String url) {
			this.connection = connection;
			this.url = url;
		}

		@Override
		public void run() {
			try {
				dis = connection.openDataInputStream();
				dos = connection.openDataOutputStream();
				String str;
				new OutputStreamWriter(dos);
				dos.writeUTF("Enter 1 to Get output file\nEnter 2 to Exit");
				dos.flush();
				whileloop:
				while (true) {
					BufferedReader d = new BufferedReader(new InputStreamReader(dis));
					str = d.readLine();
					System.out.println("Your Input:"+str);
					pWriter = new PrintWriter(new OutputStreamWriter(dos));
					pWriter.write("\\nYour Input: "+str+"\n");
					pWriter.flush();
					
					switch (str) {
					case "1":
						pWriter = new PrintWriter(new OutputStreamWriter(dos));
						pWriter.write("\\nYour Input: "+str+"\n");
						pWriter.flush();
						sendFile();
						break;
					case "2":
						pWriter = new PrintWriter(new OutputStreamWriter(dos));
						pWriter.write("\\nYou are disconnected from Raspberry !!\n");
						pWriter.flush();
						break whileloop;
					default:
						pWriter = new PrintWriter(new OutputStreamWriter(dos));
						pWriter.write("\\nYour Input: "+str+" is invalid !!\n");
						pWriter.flush();
						break;
					}

				}
				if (connection != null) {
					System.out.println("Connection closed");
					dis.close();
					connection.close();
					pWriter.close();
					dis = null;
					connection = null;
					pWriter = null;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		private void sendFile() {
			try {
				System.out.println("Service URL::" + url);
				ClientSession clientSession = (ClientSession) Connector.open(url);
				HeaderSet hsConnectReply = clientSession.connect(null);
				if (hsConnectReply.getResponseCode() != ResponseCodes.OBEX_HTTP_OK) {
					System.out.println("Failed to connect");
					return;
				}
				File file = new File("/home/pi/project/sounddetection.properties");
				InputStream is = new FileInputStream(file);
				byte filebytes[] = new byte[is.available()];
				is.read(filebytes);
				is.close();

				// Read more:
				// http://bishwajeet.blogspot.com/2009/08/how-to-send-file-to-mobile-from.html#ixzz4HmKFfMvq
				// Under Creative Commons License: Attribution Non-Commercial No
				// Derivatives
				HeaderSet hsOperation = clientSession.createHeaderSet();
				// hsOperation.setHeader(HeaderSet.NAME,
				// "Raspberry11-impqry.txt");
				// hsOperation.setHeader(HeaderSet.TYPE, "text");
				hsOperation.setHeader(HeaderSet.NAME, file.getName());
				hsOperation.setHeader(HeaderSet.TYPE, "image/jpeg");
				hsOperation.setHeader(HeaderSet.LENGTH, new Long(filebytes.length));

				// Create PUT Operation
				Operation putOperation = clientSession.put(hsOperation);

				// Send some text to server
				// byte data[] = "Hello world!".getBytes("iso-8859-1");
				OutputStream os = putOperation.openOutputStream();
				os.write(filebytes);
				os.close();

				putOperation.close();

				clientSession.disconnect(null);

				clientSession.close();

			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

}
