package com.wild.detect.sample;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

import javax.bluetooth.DiscoveryAgent;
import javax.bluetooth.LocalDevice;
import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;
import javax.microedition.io.StreamConnectionNotifier;

public class OBEXPutServerAndroid {

	static final String serverUUID = "11111111111111111111111111111123";

	public static void main(String[] args) throws IOException {
		LocalDevice localDevice = LocalDevice.getLocalDevice();

		localDevice.setDiscoverable(DiscoveryAgent.GIAC); // Advertising the
															// service

		String url = "btspp://localhost:" + serverUUID + ";name=BlueToothServer";
		StreamConnectionNotifier server = (StreamConnectionNotifier) Connector.open(url);
		StreamConnection connection = null;
		while (true) {
			connection = server.acceptAndOpen(); // Wait until
			// client
			// connects
			// === At this point, two devices should be connected ===//
			new ClientThread(connection).run();
		}
	}

	static class ClientThread implements Runnable {
		StreamConnection connection;
		DataInputStream dis;
		DataOutputStream dos;
		PrintWriter pWriter;
		public ClientThread(StreamConnection connection) {
			this.connection = connection;
		}

		@Override
		public void run() {
			try {
				dis = connection.openDataInputStream();
				dos = connection.openDataOutputStream();
				String str;
				while (true) {
					BufferedReader d = new BufferedReader(new InputStreamReader(dis));
					str = d.readLine();
					System.out.println(str);
					pWriter = new PrintWriter(new OutputStreamWriter(dos));
					pWriter.write("Your Input: "+str+"\n");
					pWriter.flush();
					
					if ((str == null) || (str != null && str.equalsIgnoreCase("x")))
						break;
				}
				if (connection != null) {
					System.out.println("Connection closed");
					dis.close();
					connection.close();
					pWriter.close();
					dis = null;
					connection = null;
					pWriter = null;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		@Override
		protected void finalize() throws Throwable {
			// TODO Auto-generated method stub
			super.finalize();
			if (connection != null) {
				System.out.println("Connection closed on finalize");
				dis.close();
				pWriter.close();
				connection.close();
				dis = null;
				connection = null;
			}
		}
	}

}
